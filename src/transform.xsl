<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1" xmlns:bar="http://www.bar.org">
<xsl:template match="/">
  <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="assets/css/style.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    </head>
  <body>

    <div id="container">
      <img src="assets/images/cadit-uk-logo.png" alt="CAD-IT UK" />
      <h1 id="title">     
      </h1>
      <br />
      <h2 id="subtitle"></h2>
      <button>Show Parts List</button>

      <table>
        <tr>
          <th>Seq.</th>
          <th>Part Number</th>
          <th>Part Description</th>
          <th>Qty.</th>
        </tr>
        <xsl:for-each select="DataSet/diffgr:diffgram/NewDataSet/Sheet2_x0024_">
          <tr>
            <td>
              <xsl:value-of select="Seq_x0023_"/>
            </td>
            <td>
              <xsl:value-of select="Part_x0020_Number"/>
            </td>
            <td>
              <xsl:value-of select="Part_x0020_Description"/>
            </td>
            <td>
              <xsl:value-of select="Qty_x0023_"/>
            </td>
          </tr>
        </xsl:for-each>
      </table>
    </div>
    
    <xsl:variable name="company">
    <xsl:for-each select="DataSet/diffgr:diffgram/NewDataSet/Sheet1_x0024_">
           
      <xsl:if test="@diffgr:id = 'Sheet1$1'">
        <xsl:value-of select="cadit-uk-logo_x0023_png"/>
      </xsl:if>
    
    </xsl:for-each>
  </xsl:variable>   
    
  <xsl:variable name="title">
    <xsl:for-each select="DataSet/diffgr:diffgram/NewDataSet/Sheet1_x0024_">
           
      <xsl:if test="@diffgr:id = 'Sheet1$3'">
        <xsl:value-of select="cadit-uk-logo_x0023_png"/>
      </xsl:if>
    
    </xsl:for-each>
  </xsl:variable>
    
  <xsl:variable name="subtitle">
    <xsl:for-each select="DataSet/diffgr:diffgram/NewDataSet/Sheet1_x0024_">
           
      <xsl:if test="@diffgr:id = 'Sheet1$4'">
        <xsl:value-of select="cadit-uk-logo_x0023_png"/>
      </xsl:if>
    
    </xsl:for-each>
  </xsl:variable>
    
    <xsl:variable name="bodyfont">
    <xsl:for-each select="DataSet/diffgr:diffgram/NewDataSet/Sheet1_x0024_">
           
      <xsl:if test="@diffgr:id = 'Sheet1$9'">
        <xsl:value-of select="F3"/>
      </xsl:if>
    
    </xsl:for-each>
  </xsl:variable>
    
    
  <xsl:variable name="subtitlecolour">
    <xsl:for-each select="DataSet/diffgr:diffgram/NewDataSet/Sheet1_x0024_">
           
      <xsl:if test="@diffgr:id = 'Sheet1$10'">
        <xsl:value-of select="F3"/>
      </xsl:if>
    
    </xsl:for-each>
  </xsl:variable>
    
  <xsl:variable name="tableheaderbackgroundcolour">
    <xsl:for-each select="DataSet/diffgr:diffgram/NewDataSet/Sheet1_x0024_">
           
      <xsl:if test="@diffgr:id = 'Sheet1$11' ">
        <xsl:value-of select="F3"/>
      </xsl:if>
    
    </xsl:for-each>
  </xsl:variable> 
    
  <xsl:variable name="tablebodyevenrowsbackgroundcolour">
    <xsl:for-each select="DataSet/diffgr:diffgram/NewDataSet/Sheet1_x0024_">
           
      <xsl:if test="@diffgr:id = 'Sheet1$12'">
        <xsl:value-of select="F3"/>
      </xsl:if>
    
    </xsl:for-each>
  </xsl:variable>
    
  <xsl:variable name="tablerowhoverbackgroundcolour">
    <xsl:for-each select="DataSet/diffgr:diffgram/NewDataSet/Sheet1_x0024_">
           
      <xsl:if test="@diffgr:id = 'Sheet1$13'">
        <xsl:value-of select="F3"/>
      </xsl:if>
    
    </xsl:for-each>
  </xsl:variable>  
    
    <script src="assets/js/interactivity.js"></script>

      <script type="text/javascript" charset="utf-8">

      $(document).ready(function(){
      
          var company = '<xsl:value-of select="$company" />';
          var title = '<xsl:value-of select="$title" />';
          var subTitle = '<xsl:value-of select="$subtitle" />';
          var bodyFont = '<xsl:value-of select="$bodyfont" />';
          var subTitleColour = '<xsl:value-of select="$subtitlecolour" />';
          var tableHeaderBackgroundColour = '<xsl:value-of select="$tableheaderbackgroundcolour" />';
          var tableBodyEvenRowsBackgroundColour = '<xsl:value-of select="$tablebodyevenrowsbackgroundcolour" />';
          var tableRowHoverBackgroundColour = '<xsl:value-of select="$tablerowhoverbackgroundcolour" />'; 
   
           // Main title text.  
           $("#title").text(title);
           
           // Subtitle colour and text.
           $("#subtitle").text(subTitle);
           $("#subtitle").css('color', subTitleColour);
           
           // Body font colour.
           $(this).css('color', bodyFont);
   
           // Table header colour.
           $("th").css("background-color", tableHeaderBackgroundColour);
   
           // Table rows.
            $("tr:even").css("background-color", tableBodyEvenRowsBackgroundColour);
            $("tr:odd").css("background-color", "#fff");
  
          // Table hover. 
          $("tr").not(':first').hover(
          function () {
            $(this).css("background", tableRowHoverBackgroundColour);
          }, 
          function () {
            $(this).css("background","");
          }
        );             
    });

  </script>

  </body>
  </html>
</xsl:template>
</xsl:stylesheet>