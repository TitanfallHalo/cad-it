$("table").hide();

$(":button").click(function () {
    if ($("table").is(":visible")) {
        $("table").hide();
        $(":button").text( 'Show Parts List');
    }
    else {
        $("table").show();
        $(":button").text( 'Hide Parts List');
    }

});