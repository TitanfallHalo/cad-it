﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using ClassLibraryExcelXML;

namespace ConvertXLSXFileToXML
{
    class Program
    {
        /// <summary>
        ///  Process to create HTML file from source XSLT and Excel files.
        /// </summary>
        /// <param name="args">Excel file, XSLT file and target HTML file.</param>
        static void Main(string[] args)
        {
            try
            {
                /* NB - XLS and XSLX have different connection strings! */
                // string connStringXSL = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};"; /* XSL */
                // string connStringXLSX = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};"; /* XLSX */

                string excelFile = args[0].ToString();
                string xsltFile = args[1].ToString();
                string xmlFile = args[2].ToString() + @"\xml.xml";
                string htmlFile = args[3].ToString() + @"\input.html";

                // Derive connection string from path, and use correct values for .XLSX.
                string connString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelFile + "; Jet OLEDB:Engine Type = 5; Extended Properties =\"Excel 8.0;\""; ;

                #region Create XML File

                // Turn XLSX into XML and save.
                IExcelToXML objExcel = new XSLXToXML();
                string resultXML = objExcel.GetXML(connString);
                string resultHTML = string.Empty;

                // Overwrite if already exists.
                if (File.Exists(xmlFile))
                    File.Delete(xmlFile);

                // Write XML file using StreamWriter  
                using (StreamWriter writer = new StreamWriter(xmlFile))
                {
                    writer.Write(resultXML);
                }

                #endregion

                #region Create HTML file.

                // Creating XSLCompiled object.
                XslCompiledTransform objXSLTransform = new XslCompiledTransform();
                objXSLTransform.Load(xsltFile);

                // Creating StringBuilder object to hold html data and creates TextWriter object to hold data from XslCompiled.Transform method.  
                StringBuilder htmlOutput = new StringBuilder();
                TextWriter htmlWriter = new StringWriter(htmlOutput);

                // Creating XmlReader object to read XML content    
                XmlReader reader = XmlReader.Create(xmlFile);

                // Call Transform() method to create html string and write in TextWriter object.    
                objXSLTransform.Transform(reader, null, htmlWriter);
                resultHTML = htmlOutput.ToString();

                // Closing xmlreader object.
                reader.Close();

                // Overwrite if already exists.
                if (File.Exists(htmlFile))
                    File.Delete(htmlFile);

                // Write HTML file using StreamWriter  
                using (StreamWriter writer = new StreamWriter(htmlFile))
                {
                    writer.Write(resultHTML);
                }

                #endregion

                Console.WriteLine("Finished.");
                Console.ReadLine();

            }
            catch (Exception ex)
            {

                if ( ex is ExcelXMLException)
                {
                    ExcelXMLException exExcel = (ExcelXMLException)ex;
                    Console.WriteLine(exExcel.ErrorCode);
                    Console.WriteLine(exExcel.Message);

                }
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);

                Console.WriteLine("Exited with error.");
                Console.ReadLine();
            }
        }
    }
}