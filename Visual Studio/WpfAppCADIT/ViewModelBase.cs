﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace WpfAppCADIT
{
    internal class ViewModelBase
    {
        private ICommand _clickCommand;

        string destinationHTMLDir = string.Empty;
        private Process translationProcess;

        public Process TranslationProcess
        {
            get { return translationProcess; }
            set { translationProcess = value; }
        }

        public ICommand ClickCommand
        {
            get
            {
                return _clickCommand ?? (_clickCommand = new CommandHandler(() => XSLTTransformAction(), () => CanExecute));
            }
        }

        /// <summary>
        ///  One process at a time.
        /// </summary>
        public bool CanExecute
        {
            get
            {
                if (translationProcess == null)
                    return true;
                else
                {
                    return translationProcess.HasExited;
                }
            }
        }

        /// <summary>
        ///  Test contractual call to convertion app.
        /// </summary>
        public void XSLTTransformAction()
        {
            try
            {
                OpenFileDialog resExcel = new OpenFileDialog();

                var sourceFile = string.Empty;
                var translationFile = string.Empty;
                var destinationXMLDir = string.Empty;


                // Filter Excel.
                resExcel.Filter = "Excel Files|*.xlsx;";

                if (resExcel.ShowDialog() == true)
                {
                    // Get the file's path.
                    sourceFile = resExcel.FileName;

                }

                if (sourceFile == string.Empty)
                    throw new Exception("Error - Must select a file.");

                OpenFileDialog resXSLT = new OpenFileDialog();

                // Filter XSLT.
                resXSLT.Filter = "XSLT Files|*.xsl;";

                if (resXSLT.ShowDialog() == true)
                {
                    // Get the file's path.
                    translationFile = resXSLT.FileName;
                }

                if (translationFile == string.Empty)
                    throw new Exception("Error - Must select a file.");

                var dialogXMLDir = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
                if (dialogXMLDir.ShowDialog().GetValueOrDefault())
                {
                    // Get destination folder.
                    destinationXMLDir = dialogXMLDir.SelectedPath;
                }

                if (destinationXMLDir == string.Empty)
                    throw new Exception("Error - Must select a folder.");

                var dialogHTMLDir = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
                if (dialogHTMLDir.ShowDialog().GetValueOrDefault())
                {
                    // Get destination folder.
                    destinationHTMLDir = dialogHTMLDir.SelectedPath;
                }

                if (destinationHTMLDir == string.Empty)
                    throw new Exception("Error - Must select a folder.");

                var startInfo = new ProcessStartInfo
                {
                    FileName = @"ConvertXLSXFileToXML.exe",
                    UseShellExecute = true
                    
                };

                startInfo.WindowStyle = ProcessWindowStyle.Normal;
                startInfo.Arguments = sourceFile + " " + translationFile + " " + destinationXMLDir + " " + destinationHTMLDir;

                translationProcess = Process.Start(startInfo);
                translationProcess.Exited += TranslationProcess_Exited;
                translationProcess.StartInfo.UseShellExecute = true;
                translationProcess.WaitForExit(); // Waits here for the process to exit.

                /// dist
                ///     assets
                ///         css
                ///             style.css < --you should hand-code this
                ///         images
                ///             cadit - uk - logo.png
                ///         js
                ///             interactivity.js < --you should hand-code this
                ///             
                /// index.html < --your XSLT script should create this
                /// src
                ///     input.xlsx
                ///     transform.xsl < --you should hand-code this
                ///     xml.xml < --your software should create this
                ///     
                /// Documentation.docx < --you will need to manually create this
                /// README.md < --don't modify this file
            }
            catch (Exception ex)
            {
                MessageBox.Show("The following error occured: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        /// <summary>
        ///  When finished show in browser.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void TranslationProcess_Exited(object sender, System.EventArgs e)
        {
            try
            {
                // Show result in default browser.
                string target = destinationHTMLDir + @"\input.html";

                var psi = new ProcessStartInfo
                {
                    FileName = target,
                    UseShellExecute = true
                };
                Process.Start(psi);
            }
            catch(Exception ex)
            {
                MessageBox.Show("The following error occured: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}