﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ClassLibraryExcelXML
{
    /// <summary>
    ///  OleDb allows us to access functionality to read Excel file information, and place it in a dataset.
    ///  
    /// /* Important Note : */
    /// /* OLE libraties needed are downloaded with Microsoft Access Database Engine 2010 Redistributable @ */
    /// /* AccessDatabaseEngine_X64.exe = 64 bit version. */
    /// 
    /// /* https://www.microsoft.com/en-gb/download/confirmation.aspx?id=13255 */
    /// 
    /// /* This program will not work unless this is installed.
    /// 
    /// </summary>
    public static class OleDbRoutines
    {
        /// <summary>
        ///  We need to get a string of XML (for XSLT) from the dataset too. This extension method provides this.
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>String.</returns>
        private static string ToXml(this DataSet ds)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamWriter = new StreamWriter(memoryStream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(DataSet));
                    xmlSerializer.Serialize(streamWriter, ds);
                    return Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
        }

        /// <summary>
        ///  Routine to convert an Excel file to XML.
        /// </summary>
        /// <remarks>
        ///  <see cref="0X00000001"/>Throws ExcelXML Exception.
        /// </remarks>
        /// <param name="connectionString"></param>
        /// <returns>DataSet</returns>
        public static DataSet GetExcelDataSet(string connectionString, ref string refDataSetAsXMLString)
        {
            string sheetName;
            DataSet ds = new DataSet();

            try
            {
                using (OleDbConnection con = new OleDbConnection(connectionString))
                {
                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        using (OleDbDataAdapter oda = new OleDbDataAdapter())
                        {
                            cmd.Connection = con;
                            con.Open();
                            DataTable dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            for (int i = 0; i < dtExcelSchema.Rows.Count; i++)
                            {
                                sheetName = dtExcelSchema.Rows[i]["TABLE_NAME"].ToString();
                                DataTable dt = new DataTable(sheetName);
                                cmd.Connection = con;
                                cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                                oda.SelectCommand = cmd;
                                oda.Fill(dt);
                                dt.TableName = sheetName;
                                ds.Tables.Add(dt);
                            }
                        }
                    }
                    refDataSetAsXMLString = ds.ToXml();
                }
            }
            catch(Exception ex)
            {
                throw new ExcelXMLException("Failed to obtain dataset.", ex, ExcelXMLException.E_OLE_FAILED_TO_GET_DATASET_ERROR);
            }

            return ds;
        }
    }
}