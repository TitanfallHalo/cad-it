﻿using System.Data;

namespace ClassLibraryExcelXML
{
    public class XSLXToXML : IExcelToXML
    {
        /// <summary>
        ///  Return an XSLX file converted to XML.
        /// </summary>
        /// <param name="path">File path</param>
        /// <returns>string</returns>
        public string GetXML(string connectionString)
        {
            string xml = string.Empty;
            DataSet ds = OleDbRoutines.GetExcelDataSet(connectionString, ref xml);
            return xml;
        }
    }
}