﻿using System;

namespace ClassLibraryExcelXML
{
    public class ExcelXMLException : Exception
    {
        /* Error Codes */
        public static readonly UInt32 E_OLE_FAILED_TO_GET_DATASET_ERROR = 0X00000001;

        // This exception error code.
        private UInt32 _ErrorCode;

        /// <summary>
        ///  Accessor to error code.
        /// </summary>
        public UInt32 ErrorCode
        {
            get { return _ErrorCode; }
            set { _ErrorCode = value; }
        }

        /// <summary>
        ///  Error code.
        /// </summary>
        /// <param name="message">Quick error description.</param>
        /// <param name="ex">Inner exception.</param>
        /// <param name="errorCode">Exception Code.</param>
        public ExcelXMLException(string message, Exception ex, UInt32 errorCode) : base(message, ex)
        {
            ErrorCode = errorCode;
        }

    }
}