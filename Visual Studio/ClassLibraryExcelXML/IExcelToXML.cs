﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibraryExcelXML
{
    public interface IExcelToXML
    {
        string GetXML(string path);
    }
}